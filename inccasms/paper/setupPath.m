% add path to kspace_hybrid_thermo_inccasms_cd
addpath ../../ % main k-space-thermometry folder for Gmri_cart
addpath ../../util % ft/ift/etc
addpath ../util % for permutations
addpath ../../inccasms % for recon script

% For in vivo, also need FSL's BET and Nifti tools
